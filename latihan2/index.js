var obj = {
	country: "Brazil",
	win: 5,
	lose: 3,
	yellowCard: 3,
	redCard: 0,
	penalty: [1,0,0,1,0]
}

// level 1 
console.info(`National Team Name : ${obj.country}`);

// level 2
const {country, win, lose, yellowCard, redCard, penalty} = obj;
console.info(`
  Statistic ${country} in world cup
  Win: ${win},
  lose: ${lose},
  Yellow Card = ${yellowCard},
  Red Card = ${redCard}
  Penalty: ${penalty}
`);

const {country: negara, yellowCard: kuning, redCard: merah, captain = "Neymar", ...sisa} = obj;
console.info(`
  Total Card Collection ${negara}
  Captain Name: ${captain}
  Yellow Card: ${kuning} | Red Card: ${merah}
  Sisa : ${sisa}
`);

// level 3
// case 1
const newObj = {...obj};
newObj.country = "France";
newObj.captain = "Hugo Lloris";

console.info(obj);
console.info(newObj);

// case 2
const newObjBaru = {...newObj, region: "Euro"};
console.info(newObjBaru);
for (const key in newObjBaru) {
  console.info(`${key}: ${newObjBaru[key]}`);
}

const makanan = {
  nama: "Coto Makassar",
  asal: "Makassar"
};

const rasaMakanan = {
  rasa: "Gurih"
};

const detailMakanan = {...makanan, ...rasaMakanan};
console.info(detailMakanan);