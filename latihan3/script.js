const worldCup = [
  {
      country: "Marocco",
      win: 5,
      lose: 3,
      worldCupWins: 0,
  },
  {
      country: "Argentina",
      win: 5,
      lose: 3,
      worldCupWins: 2,
  },
  {
      country: "Crotaia",
      win: 4,
      lose: 4,
      worldCupWins: 0,
  },
  {
      country: "France",
      win: 4,
      lose: 3,
      worldCupWins: 2,
  },
];

// > Latihan Level 1
const {country, win, lose, worldCupWins} = worldCup[0];
const createUl1 = document.createElement("ul");
const createLi1 = document.createElement("li")
const createTextNode1 = document.createTextNode(`Nama Negara: ${country} | Win: ${win} | Lose: ${lose} | World Cup Wins: ${worldCupWins}`);
createLi1.appendChild(createTextNode1);
createUl1.appendChild(createLi1);
document.body.appendChild(createUl1);

// > Latihan Level 2
document.body.innerHTML = worldCup.map((wc) => {
  return `<ul>
    <li>Nama Negara: ${wc.country}</li>
    <li>Jumlah Menang: ${wc.win} Kali</li>
    <li>Jumlah Kalah: ${wc.lose} Kali</li>
    <li>Jumlah Trofi Piala Dunia: ${wc.worldCupWins} Kali</li>
  </ul>`;
});

// > Latihan Level 3 
const allValue = worldCup.map((wc) => {
  console.info(`
    Country: ${wc.country}
    Total Win: ${wc.win}
    Total Lose: ${wc.lose}
    World Cup Trophy: ${wc.worldCupWins}
  `);
});