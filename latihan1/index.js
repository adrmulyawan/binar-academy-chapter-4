let input = [6,2,4,1,3,2];

// level 1 : Menampilkan Isi Array
input.forEach((i, index) => {
  console.info(`Isi Array Index ke-${index} ${i}`);
});
// # Output: 
// Isi Array Index ke-0 6
// Isi Array Index ke-1 2
// Isi Array Index ke-2 4
// Isi Array Index ke-3 1
// Isi Array Index ke-4 3
// Isi Array Index ke-5 2

// level 2 : Sorting Array
// > Terkecil => Terbesar
input.sort().forEach((i) => {
  console.info(`Hasil Sort Array Terkecil ke Terbesar : ${i}`);
});
// Hasil Sort Array Terkecil ke Terbesar : 1
// Hasil Sort Array Terkecil ke Terbesar : 2
// Hasil Sort Array Terkecil ke Terbesar : 2
// Hasil Sort Array Terkecil ke Terbesar : 3
// Hasil Sort Array Terkecil ke Terbesar : 4
// Hasil Sort Array Terkecil ke Terbesar : 6
let terkecil = Math.min(...input);
console.info(`Nilai Terkecil: ${terkecil}`);
// Nilai Terkecil: 1


// > Terbesar => Terkecil
input.reverse().forEach((i) => {
  console.info(`Hasil Sort Array Terbesar ke Terkecil ${i}`);
});
// Hasil Sort Array Terbesar ke Terkecil 6
// Hasil Sort Array Terbesar ke Terkecil 4
// Hasil Sort Array Terbesar ke Terkecil 3
// Hasil Sort Array Terbesar ke Terkecil 2
// Hasil Sort Array Terbesar ke Terkecil 2
// Hasil Sort Array Terbesar ke Terkecil 1
let terbesar = Math.max(...input);
console.info(`Nilai terbesar: ${terbesar}`);
// Nilai Terbesar: 6

// level 3 
// > Cari angka terbesar kedua
let angkaDicari = 4
let newInput = [];
let terbesarrKe2 = input.map((i) => {
  if (i === angkaDicari) {
    return newInput.push(i);
  }
});
console.info(`Angka terbesar ke-2: ${newInput}`);
// Angka terbesar ke-2: 4

// > jumlahkan dari angka tertinggi keterendah
const urutan = input.reverse();
const total = urutan.reduce((accumulator, value) => {
  return accumulator + value;
});
console.info(`Total Penjumlahan = ${total}`);
// Total Penumlahan = 18