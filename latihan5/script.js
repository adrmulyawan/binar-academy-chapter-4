// Level 1
// > Jumlah Bintang Sesuai Input 
function showStars(star) {
  let s = ""
  for (let i = 0; i < star; i++) {
    s += "*";
  }
  return s;
}
console.info(showStars(5));
// # Output: *****

// ====================================================================

// Level 2
// > Menampilkan Bintang Perbaris (Left Triangle Pattern)
function leftTrianglePattern(star) {
  let string = "";
  for (let i = 1; i <= star; i++) {
    for (let j = 0; j < i; j++) {
      string += "*";
    }
    string += "\n";
  }
  return string;
}

console.info(leftTrianglePattern(5));
/*
  # Output:
  *
  **
  ***
  ****
  *****
*/

// ====================================================================

// Level 3 
// > Menampilkan Bintang Perbaris (Right Pascal Star Patter)
function rightPascalStarPattern(star) {
  let string = "";
  for (let i = 1; i <= star; i++) {
    for (let j = 0; j < i; j++) {
      string += "*";
    }
    string += "\n";
  }
  for (let i = 1; i <= star - 1; i++) {
    for (let j = 0; j < star - i; j++) {
      string += "*"
    }
    string += "\n";
  }
  return string;
}

console.info(rightPascalStarPattern(3));
/*
  # Output:
  *
  **
  ***
  **
  *
*/