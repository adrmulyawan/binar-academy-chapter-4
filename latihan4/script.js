let sentence = "Lorem ipsum dolor sit amet";
let word = "loremipsum"

// Level 1
// > Tampilkan Sentece Perkata
// for (let i = 0; i < sentence.length; i++) {
//   console.info(sentence[i]);
// }

// > Tampilkan word perbaris
// let words = "";
// for (let i = 0; i < word.length; i++) {
//   words += word[i] + " ";
// }
// console.info(words);
// # Output: l o r e m i p s u m 

// ====================================================================

// Level 2 
// > Sentence : tambah / hapus beberapa bagian dari kalimat
const splitString1 = sentence.split(" ");
const addNewString = [...splitString1, "keren", "banget"];
const newString = addNewString.join(" ");
console.info(newString);
// # Output: Lorem ipsum dolor sit amet keren banget

// > Word: tambah / hapus beberapa bagian dari kalimat
const newString2 = word + "dolor" + "sit";
console.info(newString2);
// # Output: loremipsumdolorsit

// ====================================================================

// Level 3
// > Reverse string
function reverseString(str) {
  return str.split("").reverse().join("");
}

console.info(reverseString(word));
// # Output: muspimerol